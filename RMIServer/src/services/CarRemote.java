package services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import productManagement.Car;
import productManagement.ICar;

public class CarRemote extends UnicastRemoteObject implements ICar{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CarRemote() throws RemoteException {
		super();
		
	}
	
	@Override
	public double computeTax(Car c)throws RemoteException {
		// Dummy formula
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(c.getEngineCapacity() > 1601) sum = 18;
		if(c.getEngineCapacity() > 2001) sum = 72;
		if(c.getEngineCapacity() > 2601) sum = 144;
		if(c.getEngineCapacity() > 3001) sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}
	
	@Override
	public double computePrice(Car c) throws RemoteException{
		double sellp=0;
		sellp=c.getPrice()-(c.getPrice()*(2015-c.getYear())/7);
		System.out.println(c);
		return sellp;
	}
	@Override
	public Car find() throws Exception{
		return new Car(2009,2000);
	}

	@Override
	public List<Car> findAll() throws Exception {
		List<Car> list=new ArrayList<Car>();
		list.add(new Car(4234,2352));
		list.add(new Car(2355,98));
		return list;
	}
	

}
