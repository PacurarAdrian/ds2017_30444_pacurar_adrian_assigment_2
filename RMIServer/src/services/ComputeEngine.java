package services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import compute.Compute;
import compute.Task;



public class ComputeEngine extends UnicastRemoteObject implements Compute {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ComputeEngine() throws RemoteException {
	        super();
	    }
	@Override
	public <T> double executeComputePrice(Task<T> t) throws RemoteException {
		
		return t.computePrice();
	}
	@Override
	public <T> double executeComputeTax(Task<T> t) throws RemoteException {
		
		return t.computeTax();
	}
	public String hello() throws Exception{
		return "Hello world";
	}
	public int sum(int a,int b) throws Exception{
		return a+b;
	}
}

