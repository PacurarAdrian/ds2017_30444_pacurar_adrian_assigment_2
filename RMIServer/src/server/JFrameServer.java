package server;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import compute.Compute;
import productManagement.ICar;
import services.CarRemote;
import services.ComputeEngine;

import org.eclipse.swt.widgets.Button;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

public class JFrameServer {
	protected Shell shlServer;
	protected int port;
	private static int DEFAULT=1099;
	private Text textPort;
	protected List list_1;
	private Label lblStatus;
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			JFrameServer window = new JFrameServer();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlServer.open();
		shlServer.layout();
		while (!shlServer.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlServer = new Shell();
		shlServer.setSize(341, 231);
		shlServer.setText("Server");
		
		Button btnStart = new Button(shlServer, SWT.NONE);
		btnStart.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					
					port=Integer.parseInt(textPort.getText());
					LocateRegistry.createRegistry(port);
					ICar icar=new CarRemote();
					Naming.rebind("rmi://localhost:"+port+"/CarServer", icar);
					//Compute icomp=new ComputeEngine();
					//Naming.rebind("rmi://localhost:"+port+"/ComputeServer", icomp);
					
					list_1.removeAll();
					
					list_1.add("Server is ready.");
					
				} catch (Exception e2) {
					System.out.println(e2.getMessage());
					
					list_1.removeAll();
					
					list_1.add(e2.getMessage());
				}
			}
		});
		btnStart.setBounds(179, 10, 75, 25);
		btnStart.setText("Start");
		
		Label lblPort = new Label(shlServer, SWT.NONE);
		lblPort.setBounds(20, 15, 28, 15);
		lblPort.setText("Port:");
		
		list_1 = new List(shlServer, SWT.BORDER);
		list_1.setBounds(44, 90, 210, 68);
		
		textPort = new Text(shlServer, SWT.BORDER);
		textPort.setBounds(56, 12, 76, 21);
		textPort.setText(""+DEFAULT);
		
		lblStatus = new Label(shlServer, SWT.NONE);
		lblStatus.setBounds(20, 69, 55, 15);
		lblStatus.setText("Status:");

	}
}
