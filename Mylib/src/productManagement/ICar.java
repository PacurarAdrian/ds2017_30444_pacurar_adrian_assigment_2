package productManagement;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ICar extends Remote{

	public Car find() throws Exception;
	public List<Car> findAll()throws Exception;
	public double computeTax(Car c) throws RemoteException;;
	
	public double computePrice(Car c) throws RemoteException;;
}
