package compute;

import java.io.Serializable;

import productManagement.Car;

public class Service implements Task<Car>, Serializable { 

	
	private Car c;
	
	public Service(Car c)
	{
		this.c=c;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public double computeTax() {
		// Dummy formula
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(c.getEngineCapacity() > 1601) sum = 18;
		if(c.getEngineCapacity() > 2001) sum = 72;
		if(c.getEngineCapacity() > 2601) sum = 144;
		if(c.getEngineCapacity() > 3001) sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}
	
	@Override
	public double computePrice() {
		double sellp=0;
		double price=c.getPrice();
		sellp=price-(price/7)*(2015-c.getYear());
		return sellp;
	}
}