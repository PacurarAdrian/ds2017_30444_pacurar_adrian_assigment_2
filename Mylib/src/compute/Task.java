package compute;



public interface Task<T> {
    
    double computePrice();
    double computeTax();
}
