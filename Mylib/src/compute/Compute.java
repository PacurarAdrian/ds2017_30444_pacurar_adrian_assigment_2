package compute;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Compute extends Remote {
    <T> double executeComputePrice(Task<T> t) throws RemoteException;
    <T> double executeComputeTax(Task<T> t) throws RemoteException;
    public String hello() throws Exception;
	public int sum(int a,int b) throws Exception;
}

