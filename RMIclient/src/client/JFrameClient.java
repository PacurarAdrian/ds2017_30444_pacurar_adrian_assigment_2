package client;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import productManagement.Car;
import productManagement.ICar;


import org.eclipse.swt.widgets.Button;

import java.rmi.Naming;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Text;

import compute.Compute;
import compute.Service;
import compute.Task;

public class JFrameClient {

	Label lblResult;
	protected Shell shlClient;
	private Button btnTax;
	private Label lblYear;
	private Text textYear;
	private Text textEngine;
	private Text textPrice;
	private Label lblEngine;
	private Label lblPrice;
	private Text textPort;
	private Label lblPort;
	private static int DEFAULT=1099;
	private int port;
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			JFrameClient window = new JFrameClient();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlClient.open();
		shlClient.layout();
		while (!shlClient.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlClient = new Shell();
		shlClient.setSize(237, 244);
		shlClient.setText("Client");
		
		lblResult = new Label(shlClient, SWT.NONE);
		lblResult.setBounds(10, 72, 204, 25);
		
		btnTax = new Button(shlClient, SWT.NONE);
		btnTax.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					port=Integer.parseInt(textPort.getText());
					ICar icomp = (ICar) Naming.lookup("rmi://localhost:"+port+"/CarServer");
					//Compute icomp = (Compute) Naming.lookup("rmi://localhost:"+port+"/ComputeServer");
					Car car=new Car();
					
					car.setYear(Integer.parseInt(textYear.getText()));
					car.setEngineCapacity(Integer.parseInt(textEngine.getText()));
					car.setPrice(Integer.parseInt(textPrice.getText()));
					//Task<Car> task = new Service(car);
					//double res = icomp.executeComputeTax(task);
			        double res = icomp.computeTax(car);
			        lblResult.setText("result is :"+res);
				}catch (Exception e1) {
					lblResult.setText(e1.getMessage());
					e1.printStackTrace();
				}
				
			}
		});
		btnTax.setBounds(128, 41, 86, 25);
		btnTax.setText("Compute tax");
		
		lblYear = new Label(shlClient, SWT.NONE);
		lblYear.setBounds(10, 115, 95, 15);
		lblYear.setText("Fabrication year");
		
		textYear = new Text(shlClient, SWT.BORDER);
		textYear.setBounds(119, 112, 95, 21);
		
		Button btnPrice = new Button(shlClient, SWT.NONE);
		btnPrice.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					port=Integer.parseInt(textPort.getText());
					ICar icomp = (ICar) Naming.lookup("rmi://localhost:"+port+"/CarServer");
					//Compute icomp = (Compute) Naming.lookup("rmi://localhost:"+port+"/ComputeServer");
					Car car=new Car();
					
					car.setYear(Integer.parseInt(textYear.getText()));
					car.setEngineCapacity(Integer.parseInt(textEngine.getText()));
					car.setPrice(Integer.parseInt(textPrice.getText()));
					//Task<Car> task = new Service(car);
					//double pi = icomp.executeComputePrice(task);
			        double pi = icomp.computePrice(car);
			        lblResult.setText("result is :"+pi);
				}catch (Exception e1) {
					lblResult.setText(e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		btnPrice.setBounds(10, 41, 95, 25);
		btnPrice.setText("Compute price");
		
		lblEngine = new Label(shlClient, SWT.NONE);
		lblEngine.setBounds(10, 142, 75, 15);
		lblEngine.setText("Engine size");
		
		textEngine = new Text(shlClient, SWT.BORDER);
		textEngine.setBounds(119, 139, 95, 21);
		
		lblPrice = new Label(shlClient, SWT.NONE);
		lblPrice.setBounds(10, 167, 95, 15);
		lblPrice.setText("Purchasing price");
		
		textPrice = new Text(shlClient, SWT.BORDER);
		textPrice.setBounds(119, 164, 95, 21);
		
		textPort = new Text(shlClient, SWT.BORDER);
		textPort.setBounds(77, 7, 76, 21);
		textPort.setText(""+DEFAULT);
		
		lblPort = new Label(shlClient, SWT.NONE);
		lblPort.setBounds(10, 10, 55, 15);
		lblPort.setText("Port:");

	}
}
 